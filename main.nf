nextflow.enable.dsl=2

params.reads = false
params.fastas = false
params.output_dir = false
params.coverage_threshold = 0.9
params.identity_threshold = 0.9

include { RUN_PLASMIDFINDER_FASTQS; RUN_PLASMIDFINDER_FASTAS; COMBINE_PLASMIDFINDER_RESULTS} from './modules/plasmidfinder.nf'
workflow {
  if (params.reads && params.output_dir ){
    reads = Channel
    .fromFilePairs(params.reads)
    .ifEmpty { error "Cannot find any reads matching: ${params.reads}" }
    
    RUN_PLASMIDFINDER_FASTQS(reads, params.coverage_threshold, params.identity_threshold)
    COMBINE_PLASMIDFINDER_RESULTS(RUN_PLASMIDFINDER_FASTQS.out.collect())
  } else if (params.fastas && params.output_dir ) {
    fastas = Channel
    .fromPath(params.fastas)
    .map{ file -> tuple (file.baseName.replaceAll(/\..+$/,''), file)}
    .ifEmpty { error "Cannot find any fastas matching: ${params.fastas}" }
    
    RUN_PLASMIDFINDER_FASTAS(fastas, params.coverage_threshold, params.identity_threshold)
    COMBINE_PLASMIDFINDER_RESULTS(RUN_PLASMIDFINDER_FASTAS.out.collect())

  } else {
      error "Please specify reads or fastas and an output directory with --reads or --fastas and --output_dir"
  }
}