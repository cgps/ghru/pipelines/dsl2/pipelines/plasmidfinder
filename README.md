## Instructions
#### Install requirements
1. Nextflow
2. Docker
#### Get workflow and Docker container
```
git clone https://gitlab.com/cgps/ghru/pipelines/dsl2/pipelines/plasmidfinder.git
docker pull bioinformant/plasmidfinder:latest
```
#### Running the pipeline
```
nextflow run /path/to/plasmidfinder/main.nf --fastas "/path/to/*.fasta" --output_dir /path/to/output_dir -resume
```
Optionally the `--coverage_threshold` and `--identity_threshold` can be used