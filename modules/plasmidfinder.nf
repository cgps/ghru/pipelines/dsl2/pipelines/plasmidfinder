params.output_dir = false

process RUN_PLASMIDFINDER_FASTQS {
  tag "$id"

  input:
    tuple(val(id), path(reads))
    val(coverage_threshold)
    val(identity_threshold)
  output:
    path("${id}.tsv")
  script:
    """
    plasmidfinder.py -i ${reads} -o . -l ${coverage_threshold} -t ${identity_threshold} -x
    # add column for id
    awk '{FS=OFS="\\t";print "${id}", \$0}' results_tab.tsv > ${id}.tsv
    """
}


process RUN_PLASMIDFINDER_FASTAS {
  tag "$id"

  input:
    tuple(val(id), path(fasta))
    val(coverage_threshold)
    val(identity_threshold)
  output:
    path("${id}.tsv")
  script:
    """
    plasmidfinder.py -i ${fasta} -o . -l ${coverage_threshold} -t ${identity_threshold} -x
    # add column for id
    awk '{FS=OFS="\\t";print "${id}", \$0}' results_tab.tsv > ${id}.tsv
    """
}

process COMBINE_PLASMIDFINDER_RESULTS {
    publishDir params.output_dir, mode: 'copy'
    input:
      path(result_files)

    output:
      path("combined_results.tsv")

    script:
      """
        echo "ID\tDatabase\tPlasmid Identity\tQuery / Template length Contig\tPosition in contig\tNote\tAccession number" > combined_results.tsv
        for file in ${result_files}; do
          tail -n +2 \${file} >> combined_results.tsv
        done
      """
}
